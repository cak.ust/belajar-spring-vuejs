import Vue from "vue"
import Router from "vue-router"
// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

const KelasList =() => import("@/views/item/KelasList")
const RuangList =() => import("@/views/item/RuangList")
const Login = () => import('@/views/pages/Login')

Vue.use(Router)

export default new Router({
    mode: "history",
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: "/",
            name: "home",
            "alias": "/home",
            component: DefaultContainer,
            children:[
                {
                    path: "/master/kelas",
                    name: "Master Kelas",
                    "alias": "/kelas-list",
                    component: KelasList
                },
                {
                    path: "/master/ruang",
                    name: "Master Ruang",
                    "alias": "/ruang-list",
                    component: RuangList
                },
            ]
        },
        {
            path: '/',
            redirect: '/404',
            name: 'Pages',
            component: {
              render (c) { return c('router-view') }
            },
            children: [
              {
                path: 'login',
                name: 'Login',
                component: Login
              }
            ]
          }
        
    ]
})