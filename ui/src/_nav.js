export default {
    items:[
        {
            name: 'Home',
            url: '/home',
            icon: 'icon-speedometer',
            badge: {
                variant: 'primary',
                text:'NEW'
            }
        },
        {
            title: true,
            name: 'Master',
            class: '',
            wrapper: {
              element: '',
              attributes: {}
            }
          },
          {
            name: 'Kelas',
            url: '/master/kelas',
            icon: 'icon-drop'
          },
          {
            name: 'Ruang',
            url: '/master/ruang',
            icon: 'icon-pencil'
          },
    ]
}