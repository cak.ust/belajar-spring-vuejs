/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.belajar.springvue.belajarspringvuejs.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ustadho
 */
@Entity
@Table(name = "m_kelas")
@Data
public class Kelas implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "kode_kelas", nullable = false, unique = true)
    private String kode;
    
    @Column(name = "nama_kelas", nullable = false, unique = true)
    private String nama;

    
}
