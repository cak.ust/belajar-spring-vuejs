package com.gsi.belajar.springvue.belajarspringvuejs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarSpringVuejsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarSpringVuejsApplication.class, args);
	}
}
