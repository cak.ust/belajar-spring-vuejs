/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.belajar.springvue.belajarspringvuejs.resource;

import com.gsi.belajar.springvue.belajarspringvuejs.domain.Kelas;
import com.gsi.belajar.springvue.belajarspringvuejs.repository.KelasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author ustadho
 */
@RequestMapping("api/master/kelas")
@RestController
public class KelasResource {
    
    
    private final KelasRepository kelasRepository;

    public KelasResource(KelasRepository kelasRepository) {
        this.kelasRepository = kelasRepository;
    }
    
    @GetMapping("/all")
    public Iterable<Kelas> findaAll(){
        return kelasRepository.findAll();
    }
    
    
    @GetMapping()
    public Page<Kelas> findaAll(Pageable pg){
        return kelasRepository.findAll(pg);
    }
    
    @PostMapping
    public Kelas insert(@RequestBody Kelas k){
        return kelasRepository.save(k);
    }
    
    
    @PutMapping
    public Kelas update(@RequestBody Kelas k){
        return kelasRepository.save(k);
    }
    
    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id){
        kelasRepository.delete(id);
    }
}
