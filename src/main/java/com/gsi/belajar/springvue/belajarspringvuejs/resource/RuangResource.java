/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.belajar.springvue.belajarspringvuejs.resource;

import com.gsi.belajar.springvue.belajarspringvuejs.domain.Ruang;
import com.gsi.belajar.springvue.belajarspringvuejs.repository.RuangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ustadho
 */

@RequestMapping("api/master/ruang")
@RestController
public class RuangResource {
    
    
    private final RuangRepository kelasRepository;

    public RuangResource(RuangRepository kelasRepository) {
        this.kelasRepository = kelasRepository;
    }
    
    @GetMapping("/all")
    public Iterable<Ruang> findaAll(){
        return kelasRepository.findAll();
    }
    
    
    @GetMapping()
    public Page<Ruang> findaAll(Pageable pg){
        return kelasRepository.findAll(pg);
    }
    
    @PostMapping
    public Ruang insert(@RequestBody Ruang k){
        return kelasRepository.save(k);
    }
    
    
    @PutMapping
    public Ruang update(@RequestBody Ruang k){
        return kelasRepository.save(k);
    }
    
    @DeleteMapping
    public void delete(Integer id){
        kelasRepository.delete(id);
    }
}
