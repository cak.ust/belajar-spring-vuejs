/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.belajar.springvue.belajarspringvuejs;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ustadho
 */
@RestController
public class HelloController {
    
    @RequestMapping("halo")
    public String halo(){
        return "Haloo Greatsoft!!!";
    }
}
