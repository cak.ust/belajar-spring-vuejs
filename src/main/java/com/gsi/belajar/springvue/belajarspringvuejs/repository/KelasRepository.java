/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.belajar.springvue.belajarspringvuejs.repository;

import com.gsi.belajar.springvue.belajarspringvuejs.domain.Kelas;
import java.io.Serializable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author ustadho
 */
public interface KelasRepository extends PagingAndSortingRepository<Kelas, Serializable>{
    
}
