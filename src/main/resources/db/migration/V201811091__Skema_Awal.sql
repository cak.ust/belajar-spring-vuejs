create table m_kelas(
    id serial primary key,
    kode_kelas varchar(255) not null unique, 
    nama_kelas varchar(255) not null unique
);